﻿using System.ComponentModel;

namespace ListUsers.Helper
{
    public enum FilterOption
    {   [Description("Везде")]
        ByAll,
        [Description("По имени")]
        ByName,
        [Description("По фамилии")]
        BySecondName,
        [Description("По почте")]
        ByEmail,
        [Description("По телефону")]
        ByPhone
    }
}