﻿using System.Windows;
using ListUsers.ViewModels;

namespace ListUsers.View
{
    public partial class AddEditUserWindow : Window
    {
        public AddEditUserWindow(AddEditUserViewModel addEditUserViewModel)
        {
            InitializeComponent();
            addEditUserViewModel.SetCloseAction(this.Close);
            DataContext = addEditUserViewModel;
        }
    }
}