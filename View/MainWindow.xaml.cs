﻿    using ListUsers.ViewModels;

    namespace ListUsers.View
    {
        /// <summary>
        /// Interaction logic for MainWindow.xaml
        /// </summary>
        public partial class MainWindow
        {
            public MainWindow()
            {
                InitializeComponent();
                DataContext = new UserViewModel();
            }
        }
    }