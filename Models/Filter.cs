﻿using ReactiveUI;

namespace ListUsers.Models
{
    public class Filter: ReactiveObject
    {
        private string _filterName;
        public string FilterName
        {
            get => _filterName;
            set => this.RaiseAndSetIfChanged(ref _filterName, value);
        }

        private string _filterSecondName;
        public string FilterSecondName
        {
            get => _filterSecondName;
            set => this.RaiseAndSetIfChanged(ref _filterSecondName, value);
        }

        private string _filterEmail;
        public string FilterEmail
        {
            get => _filterEmail;
            set => this.RaiseAndSetIfChanged(ref _filterEmail, value);
        }

        private string _filterPhone;
        public string FilterPhone
        {
            get => _filterPhone;
            set => this.RaiseAndSetIfChanged(ref _filterPhone, value);
        }
    }
}