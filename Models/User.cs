﻿using Newtonsoft.Json;
using ReactiveUI;

namespace ListUsers.Models
{
    public class User : ReactiveObject
    {
        private string _name;
        [JsonProperty("Name")]
        public string Name
        {
            get => _name;
            set => this.RaiseAndSetIfChanged(ref _name, value);
        }
        
        private string _secondName;
        [JsonProperty("SecondName")]
        public string SecondName
        {
            get => _secondName;
            set => this.RaiseAndSetIfChanged(ref _secondName, value);
        }
        
        private string _email;
        [JsonProperty("Email")]
        public string Email
        {
            get => _email;
            set => this.RaiseAndSetIfChanged(ref _email, value);
        }
        
        private string _phone;
        [JsonProperty("Phone")]
        public string Phone
        {
            get => _phone;
            set => this.RaiseAndSetIfChanged(ref _phone, value);
        }
        
        
    }
}