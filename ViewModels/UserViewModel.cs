﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows;
using ListUsers.Helper;
using ListUsers.Models;
using ListUsers.View;
using Newtonsoft.Json;
using ReactiveUI;

namespace ListUsers.ViewModels
{
    public class UserViewModel : ReactiveObject
    {
        private const string path = "users.json";

        private ObservableCollection<User> _users;

        public ObservableCollection<User> Users
        {
            get => _users;
            set => this.RaiseAndSetIfChanged(ref _users, value);
        }

        public List<FilterOption> FilterOptions { get; set; }

        private FilterOption _selectedFilterOption;

        public FilterOption SelectedFilterOption
        {
            get => _selectedFilterOption;
            set => this.RaiseAndSetIfChanged(ref _selectedFilterOption, value);
        }

        private string _filterText;

        public string FilterText
        {
            get => _filterText;
            set => this.RaiseAndSetIfChanged(ref _filterText, value);
        }

        private ObservableCollection<User> _filteredUsers;

        public ObservableCollection<User> FilteredUsers
        {
            get => _filteredUsers;
            set => this.RaiseAndSetIfChanged(ref _filteredUsers, value);
        }

        public ReactiveCommand<Unit, Unit> AddUserCommand { get; }
        public ReactiveCommand<User, Unit> EditUserCommand { get; }
        public ReactiveCommand<User, Unit> DeleteUserCommand { get; }
        public ReactiveCommand<string, Unit> FilterCommand { get; }


        public UserViewModel()
        {
            var mainWindow = Application.Current.MainWindow;
            FilterOptions =
                new List<FilterOption>((FilterOption[])Enum.GetValues(typeof(FilterOption)));

            string json = File.ReadAllText(path);
            Users = JsonConvert.DeserializeObject<ObservableCollection<User>>(json);
            AddUserCommand = ReactiveCommand.Create(AddUser);
            EditUserCommand = ReactiveCommand.Create<User>(EditUser);
            DeleteUserCommand = ReactiveCommand.Create<User>(DeleteUser);
            FilterCommand = ReactiveCommand.Create<string>(FilterUsersCommand);

            this.WhenAnyValue(x => x.FilterText)
                .Throttle(TimeSpan.FromMilliseconds(500))
                .DistinctUntilChanged()
                .InvokeCommand(FilterCommand);
            
            mainWindow.Closing += (sender, args) =>
            {
                Save();
            };
        }

        public void AddUser()
        {
            var addEditUserViewModel = new AddEditUserViewModel();
            var addEditUserWindow = new AddEditUserWindow(addEditUserViewModel);

            var windowClosed = Observable.FromEventPattern(addEditUserWindow, "Closed");
            windowClosed.Subscribe(args =>
            {
                if (addEditUserViewModel.DialogResult == true)
                {
                    Users.Add(addEditUserViewModel.User);
                }
            });
            addEditUserWindow.ShowDialog();
        }

        public void DeleteUser(User user)
        {
            MessageBoxResult result = MessageBox.Show(
                $"Вы действительно хотите удалить пользователя {user.Name} {user.SecondName}?",
                "Удаление пользователя",
                MessageBoxButton.YesNo,
                MessageBoxImage.Warning);

            if (result == MessageBoxResult.Yes)
            {
                Users.Remove(user);
            }
        }

        public void EditUser(User user)
        {
            User editUser = new User
            {
                Name = user.Name,
                SecondName = user.SecondName,
                Email = user.Email,
                Phone = user.Phone,
            };

            var addEditUserViewModel = new AddEditUserViewModel(editUser);
            var addEditUserWindow = new AddEditUserWindow(addEditUserViewModel);

            var windowClosed = Observable.FromEventPattern(addEditUserWindow, "Closed");
            windowClosed.Subscribe(args =>
            {
                if (addEditUserViewModel.DialogResult == true)
                {
                    int index = Users.IndexOf(user);
                    Users[index] = addEditUserViewModel.User;
                }
            });
            addEditUserWindow.ShowDialog();
        }

        private ObservableCollection<User> FilterUsers(FilterOption filterOption, string filterText)
        {
            var filteredUsers = Users;

            switch (filterOption)
            {
                case FilterOption.ByName:
                    filteredUsers =
                        new ObservableCollection<User>(filteredUsers.Where(u =>
                            u.Name.ToLower().Contains(filterText.ToLower())));
                    break;
                case FilterOption.BySecondName:
                    filteredUsers =
                        new ObservableCollection<User>(filteredUsers.Where(u =>
                            u.SecondName.ToLower().Contains(filterText.ToLower())));
                    break;
                case FilterOption.ByEmail:
                    filteredUsers =
                        new ObservableCollection<User>(filteredUsers.Where(u =>
                            u.Email.ToLower().Contains(filterText.ToLower())));
                    break;
                case FilterOption.ByPhone:
                    filteredUsers =
                        new ObservableCollection<User>(filteredUsers.Where(u =>
                            u.Phone.ToLower().Contains(filterText.ToLower())));
                    break;
                case FilterOption.ByAll:
                    filteredUsers = new ObservableCollection<User>(filteredUsers.Where(u =>
                        u.Name.ToLower().Contains(filterText.ToLower()) ||
                        u.SecondName.ToLower().Contains(filterText.ToLower()) ||
                        u.Email.ToLower().Contains(filterText.ToLower()) ||
                        u.Phone.ToLower().Contains(filterText.ToLower())));
                    break;
            }

            return filteredUsers;
        }

        private void FilterUsersCommand(string filterText)
        {
            if (SelectedFilterOption != null && !string.IsNullOrEmpty(filterText))
            {
                FilteredUsers = new ObservableCollection<User>(FilterUsers(SelectedFilterOption, filterText));
            }
            else
            {
                FilteredUsers = Users;
            }
        }

        public void Save()
        {
            string json = JsonConvert.SerializeObject(Users, Formatting.Indented);
            File.WriteAllText(path, json);
        }
    }
}