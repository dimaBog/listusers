﻿using System;
using System.Reactive;
using System.Windows;
using ListUsers.Models;
using ListUsers.View;
using ListUsers.ViewModels;
using ReactiveUI;

namespace ListUsers.ViewModels
{
    public class AddEditUserViewModel: ReactiveObject
    {
        
        private User _user;
        public User User
        {
            get => _user;
            set => this.RaiseAndSetIfChanged(ref _user, value);
        }
        
        public ReactiveCommand<Unit, Unit> SaveUserCommand { get; }
        public bool? DialogResult { get; private set; }
        
        private Action CloseAction;

        public AddEditUserViewModel()
        {
            User = new User();
            SaveUserCommand = ReactiveCommand.Create(SaveUser);
        }
        
        public AddEditUserViewModel(User user)
        {
            User = user;
            SaveUserCommand = ReactiveCommand.Create(SaveUser);
        }

        private void SaveUser()
        {
            if (string.IsNullOrWhiteSpace(User.Name) || string.IsNullOrWhiteSpace(User.Email))
            {
                MessageBox.Show("Пожалуйста заполни все поля");
                return;
            }
            
            if (!IsValidEmail(User.Email))
            {
                MessageBox.Show("Пожалуйста введидите почту правильно");
                return;
            }
            
            if (User.Email.ToLower().Contains("spam"))
            {
                MessageBox.Show("Адрес электронной почты не может содержать слово 'spam'");
                return;
            }

            DialogResult = true;
            CloseAction();
        }

        public void SetCloseAction(Action closeAction)
        {
            CloseAction = closeAction;
        }
        
        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}